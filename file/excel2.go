package main

import (
	"github.com/plandem/xlsx"
)

func main() {
	xl, err := xlsx.Open("./Book3.xlsx")
	if err != nil {
		panic(err)
	}

	defer func() {
		_ = xl.Close()
	}()

	//redBoldYellow := xl.AddFormatting(
	//	format.NewStyles(
	//		format.Font.Bold,
	//		format.Font.Color("#ff0000"),
	//		format.Fill.Type(format.PatternTypeSolid),
	//		format.Fill.Color("#FFFF00"),
	//	),
	//)

	//iterating via indexes
	sheet := xl.Sheet(1)

	//Merged Cells
	_= sheet.Range("A1:A3").Merge()
	//sheet.Range("A1:C3").Split()
	//iMaxCol, iMaxRow := sheet.Dimension()
	//for iRow := 0; iRow < iMaxRow; iRow++ {
	//	for iCol := 0; iCol < iMaxCol; iCol++ {
	//		if iRow % 2 == 0 && iCol % 2 == 0 {
	//			cell := sheet.Cell(iCol, iRow)
	//			cell.SetFormatting(redBoldYellow)
	//		}
	//	}
	//}
	//
	////iterating via iterators
	//for rows := sheet.Rows(); rows.HasNext(); {
	//	_, row := rows.Next()
	//
	//	for cells := row.Cells(); cells.HasNext(); {
	//		iCol, iRow, cell := cells.Next()
	//		if iRow % 2 == 0 && iCol % 2 == 0 {
	//			cell.SetFormatting(redBoldYellow)
	//		}
	//	}
	//}
	//
	////walk through the range's cells
	//for rows := sheet.Rows(); rows.HasNext(); {
	//	_, row := rows.Next()
	//	row.Walk(func(idx, iCol, iRow int, cell *xlsx.Cell) {
	//		if iRow % 2 == 0 && iCol % 2 == 0 {
	//			cell.SetFormatting(redBoldYellow)
	//		}
	//	})
	//}
	//
	////Add hyperlink and set value same time
	//_ = sheet.CellByRef("A1").SetValueWithHyperlink("Link To Google", "http://google.com")
	//
	////Add hyperlink as string
	//_ = sheet.Range("B1:C3").SetHyperlink("spam@spam.it")
	//
	////Add hyperlink via helper type for advanced settings
	//_ = sheet.CellByRef("A7").SetHyperlink(types.NewHyperlink(
	//	types.Hyperlink.ToFile("./example_simple1.xlsx"),
	//	types.Hyperlink.ToRef("C3", "Sheet1"),
	//	types.Hyperlink.Tooltip("That's a tooltip"),
	//	types.Hyperlink.Display("Something to display"), //Cell still holds own value
	//	types.Hyperlink.Formatting(redBoldYellow),
	//))
	//
	//sheet.CellByRef("A1").RemoveHyperlink()
	//
	//
	//
	////Rich Text
	//_= sheet.CellByRef("F10").SetText(
	//	"plain text",
	//	format.NewStyles(
	//		format.Font.Bold,
	//		format.Font.Color("#ff0000"),
	//	),
	//	"red bold text",
	//	"another plain text",
	//)
	//
	////Conditional formatting
	//_= sheet.AddConditional(format.NewConditions(
	//	format.Conditions.Rule(
	//		format.Condition.Type(format.ConditionTypeCellIs),
	//		format.Condition.Operator(format.ConditionOperatorLessThanOrEqual),
	//		format.Condition.Priority(2),
	//		format.Condition.Formula("500"),
	//		format.Condition.Style(format.NewStyles(
	//			format.Font.Bold,
	//			format.Font.Color("#FF0000"),
	//		)),
	//	),
	//), "A1:A10", "B2", "C1:C10")
	//
	_= xl.SaveAs("./Book3.xlsx")
}