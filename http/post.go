package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type Response struct {
	Code int `json:"code"`
	Msg string `json:"msg"`
	DataDetail struct {
		Data *Data `json:"data"`
	}
}

type Resp struct {
	AccessToken string `json:"access_token"`
}

type Data struct {
	A string `json:"aaa"`
	Token string `json:"token"`
}

type WeChatGetCustomerArgs struct {
	ToUser string `form:"touser" json:"touser"`
	MsgType string `form:"msgtype" json:"msgtype"`
	Text string `form:"text" json:"text"`
	Content struct{
		Content string `json:"content"`
	}
}



func main()  {

	resp, err := http.Get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx3e64a6a69ac396c2&secret=e149f19d7449020539a5141bb104819b")
	if err != nil {
		fmt.Println("resp err=", err)
	}
	defer resp.Body.Close()

	fmt.Println(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("body err=", err)
	}

	fmt.Println([]byte(body))



	Resp := new(Resp)
	json.Unmarshal([]byte(body),Resp)

	fmt.Println(Resp.AccessToken)


	v := url.Values{}
	v.Add("access_token", Resp.AccessToken)
	v.Add("touser", "o0rhUwVJsQTEG0IYgYaNgYlEpbls")
	v.Add("msgtype", "text")
	v.Add("text", "{\"content\":\"Hello ceshi\"}")
	//v.Add("", "\"content\":\"Hello World\"")
	//v.Set("password", "123456")
	u := ioutil.NopCloser(strings.NewReader(v.Encode()))
	r, err := http.Post("https://api.weixin.qq.com/cgi-bin/message/custom/send", "application/x-www-form-urlencoded", u)
	if err != nil {
		fmt.Println("http post err:", err)
	}
	defer r.Body.Close()
	fmt.Println(r.StatusCode)
	body, err = ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("http body err:", err)
	}
	fmt.Println(string(body))

}
